﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using UCA.WebCalculator.WebUi.Controllers;
using UCA.WebCalculator.WebUi.Models;

namespace UCA.WebCalculator.Tests.Presentation
{
    [TestClass]
    public class CalculatorControllerFixture
    {
        private CalculatorController _SystemUnderTest;

        public CalculatorController SystemUnderTest
        {
            get
            {
                if (_SystemUnderTest == null)
                {
                    _SystemUnderTest = new CalculatorController();
                }
                return _SystemUnderTest;
            }
        }

        [TestMethod]
        public void CalculatorController_Index_ModelIsNotNull()
        {
            var actual =
                UnitTestUtility.GetModel<CalculatorViewModel>(
                    SystemUnderTest.Index());

            Assert.IsNotNull(actual, "Model was null.");
        }

        [TestMethod]
        public void CalculatorController_Index_Model_Value1IsInitialized()
        {
            var model =
                UnitTestUtility.GetModel<CalculatorViewModel>(
                    SystemUnderTest.Index());

            var actual = model.Value1;

            var expected = 0d;

            Assert.AreEqual<double>(expected, actual, "Value1 field value was wrong.");
        }
    }
}
